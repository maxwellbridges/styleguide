+++
title = "Multimedia"
date =  2018-02-14T14:49:49-05:00
weight = 9
pre = "<b>6. </b>"
chapter = true
+++

# Multimedia

Guidelines for videos, narration, podcasts, and more.