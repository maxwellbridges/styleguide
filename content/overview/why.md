+++
title = "Rationale"
date =  2018-02-13T17:24:05-05:00
weight = 3
+++

In software development, great technical content is crucial to success. Whether you're making an app or an API, no one is going to adopt something that they can't understand. 

This guide sets forth some rules and guidelines to help make sure that everyone who produces technical content in our organization does so from the same place. We produce a lot of content in a lot of different forms, and making it all consistent helps our customers learn and enjoy our products. 

Note that there is nothing in the guide about marketing copy. Marketing communications are a different world, and they operate by some different rules. 

