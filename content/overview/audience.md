+++
title = "Audience"
date =  2018-02-13T17:24:13-05:00
weight = 4
+++

If you write technical content for the company, you're in the audience for this guide! This includes _both_ external _and_ internal content. Your friends and co-workers will thank you. 

You're also in the audience for this guide if you write technical content about the company's products. For example, if you're writing a tutorial about integrating the company's logging service with your stack for our developer community, this is the place for you. 

