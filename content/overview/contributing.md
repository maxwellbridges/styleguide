+++
title = "Contributing"
date =  2018-02-14T15:00:22-05:00
weight = 8
+++

This is an open guide, and contributions are welcome. 

## Making changes

Click **Edit this page** at the beginning of any page. You can edit the page's source on GitLab and make a pull request.

All page content is written in Markdown. 

The guide theme is a partial port of the Learn theme for Grav. 

## The approval process

All pull requests are submitted for approval to the guide's content team. If the team has questions or concerns about your pull request, they'll contact you. Otherwise, they'll promote your edits to the site promptly.