+++
title = "Accessibility"
date =  2018-02-22T09:28:46-05:00
weight = 6
+++

Keep a few simple things in mind to make your content as accessible as it can be. 

## Avoid positional terms

## Provide alt-text

## Use semantic markup

## Keep headers in order