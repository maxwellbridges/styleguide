+++
title = "Messages"
date =  2018-02-13T13:13:59-05:00
weight = 5
+++

Messages from software--especially error messages--can be alarming. Write them with care.

## Be concise

Use minimal, clear language in your messages. Don't sacrifice meaning for economy, but keep things short. 

Additionally, only describe what's relevant to the reader in the moment. Unexpected messages can be confusing enough. Don't add to the reader's cognitive load. 

**Too much:** "Your account is locked due to too many failed login attempts. You cannot access it until you reset it. [Click here](#) to reset your password. Consider enable two-factor authentication, too. To learn about 2FA, [click here](#)."

**Better:** "Your account is locked due to too many failed login attempts. [Click here to reset it](#)."

## Be helpful

When applicable, error messages should prompt readers to take next steps. Don't simply point out that something has gone wrong: tell the reader how they can recover from the error. 

**Unhelpful:** "The system cannot authorize that client."

**Better:** "The system can't authorize that client. Verify the SSH key under the **Settings** menu and try again."

## Be humane

Good messages are kind and respectful. They should never seem judgmental or rude, even if you mean it in jest. 

**Just plain mean:** "Profile picture could not be uploaded. Whoops! Maybe take a nicer picture and try again? ;)"

**Better:** "Profile picture could not be uploaded. Verify the format requirements and try again."

