+++
title = "Writing useful content"
date =  2018-02-13T17:50:10-05:00
weight = 6
+++

Good content isn't just about style and grammar. It's an ends to a mean, and that's why this is a content _design_ guide rather than a content _style_ guide. 

Keep the following commandments in mind as you work.

## Author goal-oriented content

It's unlikely that a typical reader wants to explore every last one of your product's features upon encountering it. Think through the tasks and goals that are most important to your audience. Prioritize them, and leave edge cases and expert controls for later--maybe even for other documents. This way, you get your readers' on a path to doing something productive with your product as quickly as possible.

As a practical example, think of how you would show someone how to use Microsoft Word. You wouldn't step through every button in every tab of the menu for a beginner. Rather, you might show them how to write a simple letter first.

## Practice progressive disclosure

Progressive disclosure means showing information that readers need in their current context, rather than bombarding them with details. This is related to authoring goal-oriented content, but it's often a tactical decision rather than a strategic one. Pare back information about the tasks you're walking through so that the reader is following a "happy path" toward your desired outcome. Don't bog them down with option flags and warnings unless these things are immediately relevant. 

As an exercise, think of Word again. Would you show a beginner how to format their letter using the default controls on the menu, or would you have them dive into the Layout tab and start entering varyious figures for indentation, line spacing, and more? 

## Respect readers' time and attention spans

You might think that your tutorial is a masterpiece, and maybe it is. All the same, if it takes a day to work through it, you put your readers in a difficult place. _Maybe_ they'll learn to do something they want to know about, but they'd _definitely_ like to have time for lunch. 

Be cognizant of how much time the things that you explain will take someone to read, perform, and digest. Plan regular rest points--ideally every thirty to sixty minutes--where readers can stop what they're doing, save their work, and stretch their legs. 

## Don't repeat yourself

Don't reinvent the wheel. If your article or guide depends on prerequisites that are common to other articles or guides, link out to a single common document that explains the prerequisites. 

In a practical sense, this means that if you're developing a variety of cloud applications that share a common user authentication service, don't explain how to configure authentication in every guide for each application. Rather, write up an authentication configuration guide and refer to it from them. 
