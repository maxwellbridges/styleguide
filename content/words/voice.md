+++
title = "Voice and tone"
date = 2018-02-13T10:41:28-05:00
draft = false
weight = 2
+++

_Voice_ is the personality behind prose. _Tone_ is the situation the personality's found itself in.

Think of voice and tone like elements of a stageplay. A character has certain traits that are core to their being, but those traits show themselves in different ways depending on what's going on around them. 

Voice and tone are fundamental to good technical content. In general, you should attempt to strike a balance between professional and friendly. Users respond to stiff, excessively formal prose just as well as they do to typo-riddled textspeak. Measure how you communicate so that you are clear and authoritative, but also engaging.

## Voice

_Voice_ doesn't change greatly from content type to type: it should be clear, concise, authoritative, and engaging.

When you write unattributed content, you are communicating from a position of power: you're speaking for the company. Avoid slang, compose complete sentences, and be confident. Don't apologize unless you're truly inconveniencing someone, and don't write in the first person (I/we).

This does _not_ mean that you should use excessively formal or jargon-filled language! Rather, write like you are the canonical source of what you want to share (you are!), and you're happy to share it. Be confident, but use conversational language. Remember: you're writing to help another person accomplish something.

{{% notice note %}}
When writing under your name in an official capacity, like in a blog post or on Stack Overflow, feel free to interject your own personality into your writing. Just keep it business casual.
{{% /notice %}}

## Tone

_Tone_ varies depending on why and to whom you're writing. Craft your words to speak to a specific audience working in a specific context. 

A technical tutorial for beginners should be more conversational than a reference topic for experts. It should have a lower information density, too--a page in a tutorial will cover less ground than a page in a reference topic. 

A blog post or article for software developers should look different from one for non-technical executives. Obviously, one will have more technical details than the other, but an executive might be worried about things that a lone developer is not. For example, the executive post might include information on lifetime costs, SLAs, and industry adoption. A developer post might talk about day to day work, as well as the technologies in play. 

## Examples

### Release notes

**Too casual**: "The Authorize API isn't available anymore. Sorry! [Click here to make it better](#)!"

**Better**: "The Authorize API v1.0 is no longer available. To learn how to migrate to v2.0, [click here](#)."

### Concept-based doc topic

**Wordy and stiff**: "The Acme Whizbang 2000 utilizes a custom implementation of the RQM specification (RPC 99999) to deliver first-class real-time application security scanning."

**Better**: "The Acme Whizbang 2000 scans applications for security holes in real time. It relies on a custom version of RQM. You can read more about the implementation [here](#)."

### Dev to dev blog post

**Wrong audience**: "Project Arcturus leverages cutting-edge, best-in-class technologies to help you accelerate innovation in your organization. Implement it on your teams to increase work velocity and decrease time to market." 

**Better**: "Project Arcturus includes battle-tested CICD and test automation features. When my dev team adopted it, we found ourselves with enough time to chip away at our backlog and pay down technical debt. I haven't worked a weekend in six months." 

### Error message

**Apologetic**: "Sorry! You can only use alphanumeric characters in your user name. Please enter a new user name and try again."

**Better**: "You can only use alphanumeric characters in a user name. Enter another one and try again." 