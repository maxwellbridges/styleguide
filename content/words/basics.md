+++
title = "The basics"
date =  2018-02-13T12:53:35-05:00
weight = 1
+++

Use these guidelines whenever and wherever you're writing. 

## Capitalization

Consistent capitalization makes text easier for readers to understand. Use sentence-style capitalization except when writing acronyms, titles, or proper nouns. 

**Do:** 

* Write acronyms using capital letters, e.g. "API" instead of "api" or "Api." 
* Start proper nouns with capital letters, e.g. "Linus Torvalds" or "Amazon." 

**Do not:**

* Capitalize terms because they are important to the topic you are writing. For example, instead of "Make a Request to get a new Token," write "Make a request to get a new token."

### Capitalizing titles and headers

When in doubt, _use lowercase_. This includes titles and headers. 

To title a blog post about widget production using Node.js,

**do this:**

"Full-stack widget production in Node.js"

**not this:**

"Full-Stack Widget Production in Node.js"

## Grammar 

Simplify the grammar that you use to ensure that a busy, international audience can understand your message. 

### Active vs. passive voice 

Wherever possible, use active voice. Passivity muddles your writing.

**Passive:** "The request is received and the customer information is updated."

**Active:** "The API updates the customer information after it receives a request."

### Verb tenses

Choose the right verb tenses to keep things clear and concise. Generally, use the simple present tense wherever you can. 

#### Avoid the future tense

Use the future tense sparingly in technical content. Don't pull your reader out of their current context unless it's absolutely necessary. 

**Instead of the future:** "When you query the endpoint, it will return a JSON object."

**Consider the present:** "When you query the endpoint, it returns a JSON object."

#### Keep conjugations simple

Unless absolutely necessary, avoid the present aspect and other complex verb constructions in your writing. 

_Watch out for these words:_ 

* have 
* has
* had 
* been 
* should
* would 
* will

**Instead of:** "The rate limit has been exceeded."

**Consider:** "The rate limit was exceeded."

**Or even:** "You exceeded the rate limit."

### Don't be afraid to say "you" 

Don't go out of your way to avoid writing in the second person. "You" makes things conversational, and it's often the best way to get a point across. 

**Instead of:** "The Scorpio service instance is now created. It can be used to take over the world."

**Consider:** "You now have access to an instance of the Scorpio service. Happy conquering!"

## Punctuation

A few quick notes on punctuation: 

**Do:** 

* Use Oxford or serial commas in lists. "Eats, shoots, and leaves," not "eats, shoots and leaves."
* Use parentheses, question marks, and exclamation points sparingly.

**Do not:**

* Punctuate the ends of titles, headers, or list items that are under four words long
* Use semicolons (unless you're writing code).
