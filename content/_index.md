+++
title = "ACME Content Design Guide"
date = 2018-02-13T10:10:34-05:00
draft = false
+++

# ACME Content Design Guide

Welcome to an extremely real content design guide. 

## What is this thing? 

This is a pre-pre-pre-alpha exploration of the true meaning of content design guides by ACME. It's full of rules and rules of thumb for all varieties of technical content. 

This page runs on [Hugo](https://gohugo.io/), a fast and modern static website engine written in Go. It uses a partially ported version of the [Learn theme for Grav](http://github.com/matcornic/hugo-theme-learn). The Learn theme is made for documentation.

{{% notice tip %}}The Learn theme works with a _page tree structure_ to organize content. All pages are children of other pages. Do some reading about Hugo and Grav before attempting to edit the structure of this site.
{{% /notice %}}

## Contribute to this documentation
Want to update the docs? Click the **Edit this page** link displayed at the beginning of each page, and make a pull request. All docs are written in Markdown.

{{% notice info %}}
This project is built and deployed using GitLab CI and GitLab Pages. Each commit to the master branch triggers a new build. Only authorized approvers can merge PRs to master. 
{{% /notice %}}
